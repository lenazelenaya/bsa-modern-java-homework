package com.binary_studio.uniq_in_sorted_stream;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;

public final class UniqueSortedStream {

	private UniqueSortedStream() {
	}

	public static <T> Stream<Row<T>> uniqueRowsSortedByPK(Stream<Row<T>> stream) {
		// TODO: Ваш код тут. Помните про терминальные операции! :)

		//Can write "return stream.distinct();"
		//return stream.distinct();
		//Can write this function another way
		return stream.filter(distinctById(Row::getPrimaryId));
	}

	public static <T> Predicate<Row<T>> distinctById(Function<? super Row<T>, ?> id){
		Map<Object, Boolean> map = new HashMap<>();
		return t -> map.putIfAbsent(id.apply(t), Boolean.TRUE) == null;
	}

}

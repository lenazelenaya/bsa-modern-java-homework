package com.binary_studio.tree_max_depth;

import java.util.ArrayList;
import java.util.List;

public final class DepartmentMaxDepth {

	private DepartmentMaxDepth() {
	}
	public static Integer calculateMaxDepth(Department rootDepartment) {
		return rootDepartment == null ? 0 :
				1 + rootDepartment.subDepartments.stream()
						.map(DepartmentMaxDepth::calculateMaxDepth)
						.max(Integer::compare)
						.orElse(0);
/*
	if (rootDepartment == null) return 0;

		int depth = 1;
		if (rootDepartment.subDepartments.size() == 0) return depth;

		List<Department> stack = new ArrayList<>(rootDepartment.subDepartments);
		int maxSize = 0;

		while (!stack.isEmpty()) {
			for (Department value : stack) {
				if (value != null) {
					if (value.subDepartments.size() != 0 && maxSize < value.subDepartments.size()) {
						maxSize = value.subDepartments.size();
						rootDepartment = value;
					}
				}
			}
			depth++;
			stack.clear();
			if (rootDepartment != null && rootDepartment.subDepartments.size() != 0)
				for (Department department: rootDepartment.subDepartments) {
					if (department != null)
						stack.add(department);
				}
			maxSize = 0;
			rootDepartment = null;
		}
		return depth;
		*/
	}
}

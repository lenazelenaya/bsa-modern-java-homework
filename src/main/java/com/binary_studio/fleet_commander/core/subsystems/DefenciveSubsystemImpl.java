package com.binary_studio.fleet_commander.core.subsystems;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class DefenciveSubsystemImpl implements DefenciveSubsystem {
	private String name;
	private PositiveInteger powergridConsumption;
	private PositiveInteger capacitorConsumption;
	private PositiveInteger impactReductionPercent;
	private PositiveInteger shieldRegeneration;
	private PositiveInteger hullRegeneration;

	public static DefenciveSubsystemImpl construct(String name, PositiveInteger powergridConsumption,
			PositiveInteger capacitorConsumption, PositiveInteger impactReductionPercent,
			PositiveInteger shieldRegeneration, PositiveInteger hullRegeneration) throws IllegalArgumentException {
		DefenciveSubsystemImpl impl = new DefenciveSubsystemImpl();
		impl.setName(name);
		impl.setCapacitorConsumption(capacitorConsumption);
		impl.setHullRegeneration(hullRegeneration);
		impl.setImpactReductionPercent(impactReductionPercent);
		impl.setPowergridConsumption(powergridConsumption);
		impl.setShieldRegeneration(shieldRegeneration);

		return impl;
	}

	@Override
	public PositiveInteger getPowerGridConsumption() {
		return this.powergridConsumption;
	}

	@Override
	public PositiveInteger getCapacitorConsumption() {
		return this.capacitorConsumption;
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public AttackAction reduceDamage(AttackAction incomingDamage) {
		int getDamage = incomingDamage.damage.value();
		double percent = (double)this.impactReductionPercent.value() / 100;
		if (percent > 1) percent = 0.95;
		int reducedDamage = getDamage - (int)(getDamage * percent);
		//reducedDamage = Math.max(reducedDamage, 1);
		return new AttackAction(PositiveInteger.of(reducedDamage), incomingDamage.attacker, incomingDamage.target, incomingDamage.weapon);
	}

	@Override
	public RegenerateAction regenerate() {
		return new RegenerateAction(this.getShieldRegeneration(), this.getHullRegeneration());
	}

	public void setName(String name) {
		if (name.trim().length() == 0) throw new IllegalArgumentException("Name should be not null and not empty");
		this.name = name;
	}

	public void setPowergridConsumption(PositiveInteger powergridConsumption) {
		this.powergridConsumption = powergridConsumption;
	}

	public void setCapacitorConsumption(PositiveInteger capacitorConsumption) {
		this.capacitorConsumption = capacitorConsumption;
	}

	public void setImpactReductionPercent(PositiveInteger impactReductionPercent) {
		this.impactReductionPercent = impactReductionPercent;
	}

	public PositiveInteger getShieldRegeneration() {
		return shieldRegeneration;
	}

	public void setShieldRegeneration(PositiveInteger shieldRegeneration) {
		this.shieldRegeneration = shieldRegeneration;
	}

	public PositiveInteger getHullRegeneration() {
		return hullRegeneration;
	}

	public void setHullRegeneration(PositiveInteger hullRegeneration) {
		this.hullRegeneration = hullRegeneration;
	}

	public static double round(double value, int places) {
		if (places < 0) throw new IllegalArgumentException();

		long factor = (long) Math.pow(10, places);
		value = value * factor;
		long tmp = Math.round(value);
		return (double) tmp / factor;
	}
}

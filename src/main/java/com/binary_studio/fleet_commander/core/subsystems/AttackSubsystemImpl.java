package com.binary_studio.fleet_commander.core.subsystems;

import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;

public final class AttackSubsystemImpl implements AttackSubsystem {
	private String name;
	private PositiveInteger powerGridRequirements;
	private PositiveInteger capacitorConsumption;
	private PositiveInteger optimalSize;
	private PositiveInteger optimalSpeed;
	private PositiveInteger baseDamage;

	public static AttackSubsystemImpl construct(String name, PositiveInteger powerGridRequirments,
												PositiveInteger capacitorConsumption, PositiveInteger optimalSpeed, PositiveInteger optimalSize,
												PositiveInteger baseDamage) throws IllegalArgumentException {
		AttackSubsystemImpl impl = new AttackSubsystemImpl();
		impl.setName(name);
		impl.setCapacitorConsumption(capacitorConsumption);
		impl.setPowergridRequirements(powerGridRequirments);
		impl.setOptimalSize(optimalSize);
		impl.setBaseDamage(baseDamage);
		impl.setOptimalSpeed(optimalSpeed);

		return impl;
	}

	@Override
	public PositiveInteger getPowerGridConsumption() {
		return this.powerGridRequirements;
	}

	@Override
	public PositiveInteger getCapacitorConsumption() {
		return this.capacitorConsumption;
	}

	@Override
	public PositiveInteger attack(Attackable target) {
		double sizeReductionModifier =
				target.getSize().value() >= this.getOptimalSize().value() ?
						1 : round(target.getSize().value() / (double)this.getOptimalSize().value(),2);
		double speedReductionModifier =
				target.getCurrentSpeed().value() <= this.getOptimalSpeed().value() ? 1 :
						 round(this.getOptimalSpeed().value() /
								(double)(2 * target.getCurrentSpeed().value()), 2);

		int damage = (int)(this.getBaseDamage().value() * Math.min(sizeReductionModifier, speedReductionModifier));

		return PositiveInteger.of(damage);
	}

	@Override
	public String getName() {
		return this.name;
	}

	public void setName(String name) throws IllegalArgumentException {
		if (name.trim().length() == 0) {
			throw new IllegalArgumentException("Name should be not null and not empty");
		}
		this.name = name;
	}

	public void setPowergridRequirements(PositiveInteger powergridRequirments) {
		this.powerGridRequirements = powergridRequirments;
	}

	public void setCapacitorConsumption(PositiveInteger capacitorConsumption) {
		this.capacitorConsumption = capacitorConsumption;
	}

	public void setOptimalSize(PositiveInteger optimalSize) {
		this.optimalSize = optimalSize;
	}

	public void setOptimalSpeed(PositiveInteger optimalSpeed) {
		this.optimalSpeed = optimalSpeed;
	}

	public void setBaseDamage(PositiveInteger baseDamage) {
		this.baseDamage = baseDamage;
	}

	public PositiveInteger getOptimalSize() {
		return optimalSize;
	}

	public PositiveInteger getOptimalSpeed() {
		return optimalSpeed;
	}

	public PositiveInteger getBaseDamage() {
		return baseDamage;
	}

	//Function to round double to 2 decimal places
	public static double round(double value, int places) {
		if (places < 0) throw new IllegalArgumentException();

		long factor = (long) Math.pow(10, places);
		value = value * factor;
		long tmp = Math.round(value);
		return (double) tmp / factor;
	}
}

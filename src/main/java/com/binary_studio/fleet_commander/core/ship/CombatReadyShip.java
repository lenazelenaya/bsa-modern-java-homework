package com.binary_studio.fleet_commander.core.ship;

import java.util.Optional;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.AttackResult;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.ship.contract.CombatReadyVessel;

public final class CombatReadyShip implements CombatReadyVessel {
	private DockedShip ship;
	private PositiveInteger capacitorAmount;
	private PositiveInteger shield;
	private PositiveInteger hull;

	public void setShip(DockedShip ship) {
		this.ship = ship;
		this.capacitorAmount = ship.getCapacitorAmount();
		this.shield = ship.getShieldHP();
		this.hull = ship.getHullHP();
	}

	@Override
	public void endTurn() {
		if(!this.ship.getCapacitorAmount().value().equals(this.capacitorAmount.value()))
			this.regenerate();
	}

	@Override
	public void startTurn() {
	}

	@Override
	public String getName() {
		return this.ship.getName();
	}

	@Override
	public PositiveInteger getSize() {
		return this.ship.getSize();
	}

	@Override
	public PositiveInteger getCurrentSpeed() {
		return this.ship.getSpeed();
	}

	@Override
	public Optional<AttackAction> attack(Attackable target) {
		if (this.ship.getAttackSubsystem().isPresent()) {
			PositiveInteger damage = this.ship.getAttackSubsystem().get().attack(target);
			AttackAction attackAction =
					new AttackAction(damage, this, target, this.ship.getAttackSubsystem().get());
			if (this.ship.getCapacitorAmount().value() >=
					this.ship.getAttackSubsystem().get().getCapacitorConsumption().value()) {
				PositiveInteger capacitor = PositiveInteger.of(this.ship.getCapacitorAmount().value() -
						this.ship.getAttackSubsystem().get().getCapacitorConsumption().value());
				this.ship.setCapacitorAmount(capacitor);
				return Optional.of(attackAction);
			}
		}
		return Optional.empty();
	}

	@Override
	public AttackResult applyAttack(AttackAction attack) {
		AttackAction diffAttack = this.ship.getDefenciveSubsystem().get().reduceDamage(attack);
		int damage = diffAttack.damage.value();
		int shieldHP = this.ship.getShieldHP().value();
		int hullHP = this.ship.getHullHP().value();
		if(this.ship.getDefenciveSubsystem().isPresent()) {
			if (this.ship.getShieldHP().value() > 0) {
				int newShieldHP = shieldHP - damage;
				if (newShieldHP <= 0){
					int newHullHP = hullHP + newShieldHP;
					if(newHullHP <= 0) return new AttackResult.Destroyed();
					this.ship.setHullHP(PositiveInteger.of(newHullHP));
					this.ship.setShieldHP(PositiveInteger.of(0));
				}
				if (newShieldHP > 0) this.ship.setShieldHP(PositiveInteger.of(newShieldHP));
				return new AttackResult.DamageRecived(attack.weapon, diffAttack.damage, attack.target);
			}else {
				int newHullHP = hullHP - damage;
				if(newHullHP <= 0) return new AttackResult.Destroyed();
				this.ship.setHullHP(PositiveInteger.of(newHullHP));
			}
		}
		return new AttackResult.Destroyed();
	}

	@Override
	public Optional<RegenerateAction> regenerate() {
		if (this.ship.getDefenciveSubsystem().isPresent()) {
				this.ship.setCapacitorAmount(capacity());
				//If ship has enough capacity then regenerate hull and shield
				if(this.ship.getCapacitorAmount().value() >=
						this.ship.getDefenciveSubsystem().get().getCapacitorConsumption().value()) {
					RegenerateAction regenerateAction = this.ship.getDefenciveSubsystem().get().regenerate();
					if(!this.ship.getHullHP().equals(this.hull) || !this.ship.getShieldHP().equals(this.shield)) {
						int differenceHull;
						int differenceShield;
						PositiveInteger regenShield = PositiveInteger.of(0);
						PositiveInteger regenHull = PositiveInteger.of(0);

						if (!this.ship.getHullHP().equals(this.hull)) {
							differenceHull = diffHull();
							if (differenceHull > 0 && differenceHull <= regenerateAction.hullHPRegenerated.value()) {
								regenHull = regenerateAction.hullHPRegenerated;
								this.ship.setHullHP(PositiveInteger.of(this.ship.getHullHP().value() + regenHull.value()));
							} else if (differenceHull > 0){
								regenHull = PositiveInteger.of(this.hull.value() - this.ship.getHullHP().value());
								this.ship.setHullHP(this.hull);
							}
						}

						if (!this.ship.getShieldHP().equals(this.shield)){
							differenceShield = diffShield();
							if (differenceShield > 0 && differenceShield > regenerateAction.shieldHPRegenerated.value()) {
								 regenShield = regenerateAction.shieldHPRegenerated;
								this.ship.setShieldHP(PositiveInteger.of(this.ship.getShieldHP().value() + regenShield.value()));
							} else if (differenceShield > 0){
								regenShield = PositiveInteger.of(this.shield.value() - this.ship.getShieldHP().value());
								this.ship.setShieldHP(this.shield);
							}
						}
						int newCapacityAfterRegen = this.ship.getCapacitorAmount().value() -
								this.ship.getDefenciveSubsystem().get().getCapacitorConsumption().value();
						this.ship.setCapacitorAmount(PositiveInteger.of(newCapacityAfterRegen));
						return Optional.of(new RegenerateAction(regenShield, regenHull));
					}
				}else return Optional.empty();
			}
		return Optional.of(new RegenerateAction(PositiveInteger.of(0), PositiveInteger.of(0)));
	}

	public PositiveInteger capacity(){
		PositiveInteger capacitor;
		if (this.ship.getCapacitorRechargeRate().value() + this.ship.getCapacitorAmount().value() >
				this.capacitorAmount.value())
			capacitor = this.capacitorAmount;
		else capacitor = PositiveInteger.of(this.ship.getCapacitorAmount().value() +
				this.ship.getCapacitorRechargeRate().value());
		return capacitor;
	}

	public int diffHull(){
		return this.hull.value() - this.ship.getHullHP().value();
	}

	public int diffShield(){
		return this.shield.value() - this.ship.getShieldHP().value();
	}

}

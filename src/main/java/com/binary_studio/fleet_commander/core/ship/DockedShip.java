package com.binary_studio.fleet_commander.core.ship;

import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.exceptions.InsufficientPowergridException;
import com.binary_studio.fleet_commander.core.exceptions.NotAllSubsystemsFitted;
import com.binary_studio.fleet_commander.core.ship.contract.ModularVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

import java.util.Optional;

public final class DockedShip implements ModularVessel {
	private String name;
	private PositiveInteger shieldHP;
	private PositiveInteger hullHP;
	private PositiveInteger powergridOutput;
	private PositiveInteger capacitorAmount;
	private PositiveInteger capacitorRechargeRate;
	private PositiveInteger speed;
	private PositiveInteger size;
	private Optional<AttackSubsystem> attackSubsystem = Optional.empty();
	private Optional<DefenciveSubsystem> defenciveSubsystem = Optional.empty();

	public static DockedShip construct(String name, PositiveInteger shieldHP, PositiveInteger hullHP,
			PositiveInteger powergridOutput, PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate,
			PositiveInteger speed, PositiveInteger size) {
		DockedShip ship = new DockedShip();
		ship.setName(name);
		ship.setCapacitorAmount(capacitorAmount);
		ship.setCapacitorRechargeRate(capacitorRechargeRate);
		ship.setHullHP(hullHP);
		ship.setPowergridOutput(powergridOutput);
		ship.setShieldHP(shieldHP);
		ship.setSize(size);
		ship.setSpeed(speed);

		return ship;
	}

	@Override
	public void fitAttackSubsystem(AttackSubsystem subsystem) throws InsufficientPowergridException {
		if (subsystem != null) {
			if (this.getPowergridOutput().value() < subsystem.getPowerGridConsumption().value()) {
				Integer missingPowerGrid =
						subsystem.getPowerGridConsumption().value() - this.getPowergridOutput().value();
				throw new InsufficientPowergridException(missingPowerGrid);
			}
			this.attackSubsystem = Optional.of(subsystem);
			this.powergridOutput = PositiveInteger.of(this.getPowergridOutput().value() - subsystem.getPowerGridConsumption().value());
		}
		else this.attackSubsystem = Optional.empty();
	}

	@Override
	public void fitDefensiveSubsystem(DefenciveSubsystem subsystem) throws InsufficientPowergridException {
		if (subsystem != null) {
			if (this.getPowergridOutput().value() < subsystem.getPowerGridConsumption().value()) {
				Integer missingPowergrid = subsystem.getPowerGridConsumption().value() - this.getPowergridOutput().value();
				throw new InsufficientPowergridException(missingPowergrid);
			}
			this.defenciveSubsystem = Optional.of(subsystem);
			this.powergridOutput = PositiveInteger.of(this.getPowergridOutput().value() - subsystem.getPowerGridConsumption().value());
		}
		else this.defenciveSubsystem = Optional.empty();
	}

	public CombatReadyShip undock() throws NotAllSubsystemsFitted {
		if (this.attackSubsystem.isEmpty() && this.defenciveSubsystem.isEmpty()) throw NotAllSubsystemsFitted.bothMissing();
		else if (this.defenciveSubsystem.isEmpty()) throw NotAllSubsystemsFitted.defenciveMissing();
		else if (this.attackSubsystem.isEmpty()) throw NotAllSubsystemsFitted.attackMissing();
		CombatReadyShip ship = new CombatReadyShip();
		ship.setShip(this);
		return ship;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		if (name.isEmpty()) throw new IllegalArgumentException("Name should be not null and not empty");
		this.name = name;
	}

	public PositiveInteger getShieldHP() {
		return shieldHP;
	}

	public void setShieldHP(PositiveInteger shieldHP) {
		this.shieldHP = shieldHP;
	}

	public PositiveInteger getHullHP() {
		return hullHP;
	}

	public void setHullHP(PositiveInteger hullHP) {
		this.hullHP = hullHP;
	}

	public PositiveInteger getPowergridOutput() {
		return powergridOutput;
	}

	public void setPowergridOutput(PositiveInteger powergridOutput) {
		this.powergridOutput = powergridOutput;
	}

	public PositiveInteger getCapacitorAmount() {
		return capacitorAmount;
	}

	public void setCapacitorAmount(PositiveInteger capacitorAmount) {
		this.capacitorAmount = capacitorAmount;
	}

	public PositiveInteger getCapacitorRechargeRate() {
		return capacitorRechargeRate;
	}

	public void setCapacitorRechargeRate(PositiveInteger capacitorRechargeRate) {
		this.capacitorRechargeRate = capacitorRechargeRate;
	}

	public PositiveInteger getSpeed() {
		return speed;
	}

	public void setSpeed(PositiveInteger speed) {
		this.speed = speed;
	}

	public PositiveInteger getSize() {
		return size;
	}

	public void setSize(PositiveInteger size) {
		this.size = size;
	}

	public Optional<AttackSubsystem> getAttackSubsystem() {
		return attackSubsystem;
	}

	public Optional<DefenciveSubsystem> getDefenciveSubsystem() {
		return defenciveSubsystem;
	}
}
